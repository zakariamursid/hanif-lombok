const insert = (url,type) =>
{
    $.ajax({
        url:mainpath+url,
        method:"POST",
        data:new FormData(form),
        contentType: false,
        cache:false,
        processData: false,
        dataType:"json",

        success:function(response)
        {
            if(response.status == '200')
            {
                if(url === '/insert')
                {
                    if(type === 'save-print')
                    {
                        window.location = mainpath + '/print/' +response.id;
                    }
                    else
                    {
                        success(response.message);
                    }
                }
                else if(url === '/update')
                {
                    if(type === 'save-print')
                    {
                        window.location = mainpath + '/print/' +response.id;
                    }
                    else
                    {
                        success(response.message);
                    }
                    // sucess(response.message,response.id);
                }
            }
            else
            {
                failed(response.message);
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown)
        {
            if(XMLHttpRequest.responseJSON != undefined)
            {
            failed(XMLHttpRequest.responseJSON.message);
            }
            else
            {
            failed(errorThrown);
            }
        }
   });
};

const failed = (message) =>
{
    swal({
        title: message,
        text: "",
        icon: "error",
    });    
}
const success = (message) =>
{
    swal({
        title: message,
        icon: "warning",
        buttons: true,
        dangerMode: false,
        showCancelButton: false,
        showCancelButton: false,
        showConfirmButton: true,
        confirmButtonText: "OK",
                // cancelButtonText: "Tutup",
        closeOnConfirm: false,
        closeOnCancel: false
    },

    function(isConfirm)
    {
        location.reload();
    });
}

const formatRupiah = (angka, prefix) =>
{
        var number_string = angka.replace(/[^,\d]/g, '').toString(),
        split           = number_string.split(','),
        sisa             = split[0].length % 3,
        rupiah             = split[0].substr(0, sisa),
        ribuan             = split[0].substr(sisa).match(/\d{3}/gi);

        // tambahkan titik jika yang di input sudah menjadi angka satuan ribuan
        if(ribuan){
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
        }

        rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
        return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
}

const tara = (id) =>
{
    let bruto = $("#bruto_"+id).val();
    let tara = Math.floor(bruto/100);
    let products_price = $("#products_price_"+id).val();
    let sub_total_price = 0;

    if(tara > 0)
    {
        let final = bruto - (2 * tara);
        let sub_total_price = products_price * final;

        $("#netto_"+id).val(final);
        $("#sub_total_price_"+id).val(sub_total_price);

        grand_total();
    }
    else
    {
        let sub_total_price = products_price * bruto;

        if(bruto > 0)
        {
            let final = bruto;
            // let final = bruto - 1;
            $("#netto_"+id).val(final);
        }

        $("#sub_total_price_"+id).val(sub_total_price);

        grand_total();
    }

    // console.log(tara);
}

const count = (id) =>
{
    let products_price = $("#products_price_"+id).val();
    let netto = $("#netto_"+id).val();

    let sub_total_price = products_price * netto;

    $("#sub_total_price_"+id).val(sub_total_price);

    grand_total();
}

const grand_total = () =>
{
    let sum = 0;

    for (let i = 0; i < count_product; i++) {
        let sub_total = 0;

        if($("#sub_total_price_"+i).val() == '')
        {
            sub_total = 0;
        }
        else
        {
            sub_total = $("#sub_total_price_"+i).val();
        }


        // console.log(sub_total);

        sum += parseInt(sub_total);

        // console.log(sum);

    }

    document.getElementById('text_grand_total').innerHTML= '';
    document.getElementById('text_grand_total').innerHTML= formatRupiah(sum.toString(), 'Rp. ');
}