
    const insert = (url,type) =>
    {
        $.ajax({
            url:mainpath+url,
            method:"POST",
            data:new FormData(form),
            contentType: false,
            cache:false,
            processData: false,
            dataType:"json",

            success:function(response)
            {
                if(response.status == '200')
                {
                    if(url === '/insert')
                    {
                        if(type === 'save-print')
                        {
                            window.location = mainpath + '/print/' +response.id;
                        }
                        else
                        {
                            success(response.message);
                        }
                    }
                    else if(url === '/update')
                    {
                        if(type === 'save-print')
                        {
                            window.location = mainpath + '/print/' +response.id;
                        }
                        else
                        {
                            success(response.message);
                        }
                    }
                }
                else
                {
                    failed(response.message);
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown)
            {
                if(XMLHttpRequest.responseJSON != undefined)
                {
                failed(XMLHttpRequest.responseJSON.message);
                }
                else
                {
                failed(errorThrown);
                }
            }
       });
    };

    const failed = (message) =>
    {
        swal({
            title: message,
            text: "",
            icon: "error",
        });    
    }

const success = (message) =>
{
    swal({
        title: message,
        icon: "warning",
        buttons: true,
        dangerMode: false,
        // showCancelButton: true,
        showConfirmButton: true,
        confirmButtonText: "OK",
                // cancelButtonText: "Tutup",
        closeOnConfirm: true,
        closeOnCancel: true
    },

    function(isConfirm)
    {
        location.reload();
    });
}

    const count = (id) =>
    {
        let pack = $("#pack_"+id).val();
        console.log(pack);
        let qty = $("#qty_"+id).val();
        console.log(qty);
        let koli_total = $("#koli_total_"+id).val();
        // console.log(koli_total);
        if(koli_total == '')
        {
            koli_total = 0;
        }
        
        let sub_total = Number((qty * pack)) + Number(koli_total);

        sub_total = Number(sub_total).toFixed(2);

        $("#sub_total_"+id).val(sub_total);

        grand_total();
     }

    const grand_total = () =>
    {
        let sum = 0;

        for (let i = 0; i < count_product; i++) {
            let sub_total = 0;

            if($("#sub_total_"+i).val() == '')
            {
                sub_total = 0;
            }
            else
            {
                sub_total = $("#sub_total_"+i).val();
            }

            // console.log(sub_total);

            sum += Number(sub_total);

            // console.log(sum);

        }

        sum = Number(sum).toFixed(2);

        document.getElementById('text_grand_total').innerHTML= '';
        document.getElementById('text_grand_total').innerHTML= sum.toString() +' KG';
    }

    $('#category').on('change', function(){

        val = $(this).val();

        if(val === 'Sumatera')
        {
            $("#inputExpedition").show();
        }
        else
        {
            $("#inputExpedition").hide();
        }

    });