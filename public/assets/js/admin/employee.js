    const dynamic_field = (number) => 
    {
       html = '<tr>';
       html += '<td width="10%"><input type="text" class="form-control" name="data['+number+']" placeholder="Nama"/></td>';
       html += '<td width="10%"><button type="button" name="add" id="add" class="btn btn-sm btn-success"><i class="fa fa-plus"></i></button>';
       html += ' &nbsp';
    //    html += '<td><input required type="text" name="name['+number+']" class="bg-transparent col font-sf-pro-medium p-2" style="border: 0; border-bottom: 3px solid #D8D8D8" placeholder="(Isi nama)"></td>';

        if(number > 1)
        {
            html += '<button type="button" name="remove" id="remove" class="btn btn-sm btn-danger remove"><i class="fa fa-trash"></i></button></tr>';
        }
        else
        {
            html += '';
        }

        $('#employee_table > tbody').append(html);   
    }

    $(document).on('click', '#add', function()
    {
        count++;
        dynamic_field(count);

    });

    $(document).on('click', '.remove', function()
    {
        count--;
        $(this).closest("tr").remove();
    });

    const insert = (url) =>
    {
        $.ajax({
            url:mainpath+url,
            method:"POST",
            data:new FormData(form),
            contentType: false,
            cache:false,
            processData: false,
            dataType:"json",

            success:function(response)
            {
                if(response.status == '200')
                {
                    if(url === '/insert')
                    {
                        window.location = mainpath;
                    }
                    else if(url === '/update')
                    {
                        window.location = mainpath;
                        // sucess(response.message,response.id);
                    }
                }
                else
                {
                    failed(response.message);
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown)
            {
                if(XMLHttpRequest.responseJSON != undefined)
                {
                failed(XMLHttpRequest.responseJSON.message);
                }
                else
                {
                failed(errorThrown);
                }
            }
       });
    };



    const failed = (message) =>
    {
        swal({
            title: message,
            text: "",
            icon: "error",
        });    
    }

    const success = (message) =>
    {
        swal(message, {
            icon: "success",
            timer: 1000,
            buttons: false,
            // closeOnClickOutside: false
        })
        .then(() => {
            window.location = mainpath;
        })
    }