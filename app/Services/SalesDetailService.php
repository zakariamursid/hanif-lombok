<?php
namespace App\Services;

use Illuminate\Support\Facades\DB;
use App\Repositories\Sales;
use App\Repositories\SalesDetail;

class SalesDetailService extends SalesDetail
{
    public static function getByDate($date)
    {
        $data = SalesDetail::table()->whereDate('created_at',$date)->get();
        return $data;
    }

    public static function getStatProductsByDate($date)
    {
        $data = self::getByDate($date);

        $data = collect($data)->groupBy('products_id')->toArray();

        $products = [];
        $total = 0;
        
        foreach($data as $key => $val)
        {
            $total = 0;

            foreach($val as $v)
            {
                $total += $v->sub_total;            
            }

            $products[$key] = $total;
        }

        return $products;
    }

    public static function insert($array,$sales_id)
    {
        // SalesDetail::table()->where('sales_id',$sales_id)->delete();

        foreach($array as $val)
        {
            if($val['sub_total'] != 0)
            {
                $data = new SalesDetail();
                $data->sales_id = $sales_id;
                $data->products_id = $val['products_id'];
                $data->products_name = $val['products_name'];
                // $data->products_price = $val['products_price'];
                if($val['purchase_price'])
                {
                    $data->purchase_price = $val['purchase_price'];
                }
                else
                {
                    $data->purchase_price = 0;
                }          
                if($val['products_price'])
                {
                    $data->products_price = $val['products_price'];
                }
                else
                {
                    $data->products_price = 0;
                }
                
                if($val['pack'])
                {
                    $data->pack = $val['pack'];
                }
                else
                {
                    $data->pack = 0;
                }

                if($val['qty'])
                {
                    $data->qty = $val['qty'];
                }
                else
                {
                    $data->qty = 0;
                }
                
                $data->koli = $val['koli'];
                $data->koli_total = $val['koli_total'];
                $data->sub_total = $val['sub_total'];
                $data->sub_total_price = $val['products_price'] * $val['sub_total'];
                $data->created_at = date("Y-m-d H:i:s"); //this created_at is a magic method you can ignore this
                $data->updated_at = date("Y-m-d H:i:s"); //this created_at is a magic method you can ignore this
                $data->save();
            }
        }

        return SalesDetail::findAllBy("sales_id",$sales_id);
    }

    public static function update($array,$sales_id)
    {
        SalesDetail::table()->where('sales_id',$sales_id)->delete();

        foreach($array as $val)
        {
            if($val['sub_total'] != 0)
            {
                $data = new SalesDetail();
                $data->sales_id = $sales_id;
                $data->products_id = $val['products_id'];
                $data->products_name = $val['products_name'];
                // $data->products_price = $val['products_price'];

                if($val['purchase_price'])
                {
                    $data->purchase_price = $val['purchase_price'];
                }
                else
                {
                    $data->purchase_price = 0;
                }
                if($val['products_price'])
                {
                    $data->products_price = $val['products_price'];
                }
                else
                {
                    $data->products_price = 0;
                }                
                if($val['pack'])
                {
                    $data->pack = $val['pack'];
                }
                else
                {
                    $data->pack = 0;
                }
                

                if($val['qty'])
                {
                    $data->qty = $val['qty'];
                }
                else
                {
                    $data->qty = 0;
                }

                $data->koli = $val['koli'];
                $data->koli_total = $val['koli_total'];

                $data->sub_total = $val['sub_total'];
                $data->sub_total_price = $val['products_price'] * $val['sub_total'];
                $data->created_at = date("Y-m-d H:i:s"); //this created_at is a magic method you can ignore this
                $data->updated_at = date("Y-m-d H:i:s"); //this created_at is a magic method you can ignore this
                $data->save();
            }
        }

        return SalesDetail::findAllBy("sales_id",$sales_id);
    }
}