<?php
namespace App\Services;

use Illuminate\Support\Facades\DB;
use App\Repositories\PurchaseDetail;

class PurchaseDetailService extends PurchaseDetail
{
    public static function getByDate($date)
    {
        $data = PurchaseDetail::table()->whereDate('created_at',$date)->get();
        return $data;
    }

    public static function getStatProductsByDate($date)
    {
        $data = self::getByDate($date);

        $data = collect($data)->groupBy('products_id')->toArray();
        // dd($data);

        $products = [];
        
        foreach($data as $key => $val)
        {
            $total = 0;
            $total_price = 0;

            foreach($val as $v)
            {
                $total_price += $v->sub_total_price;            
                $total += $v->netto;            
            }

            $obj = new \StdClass();
            $obj->total = $total;
            $obj->total_price = $total_price;

            $products[$key] = $obj;
        }

        return $products;
    }
    
    public static function insert($array,$purchase_id)
    {
        foreach($array as $val)
        {
            if($val['sub_total_price'] != 0)
            {
                $data = new PurchaseDetail();
                $data->purchase_id = $purchase_id;
                $data->products_id = $val['products_id'];
                $data->products_name = $val['products_name'];
                $data->products_price = $val['products_price'];
                $data->netto = $val['netto'];
                $data->bruto = $val['bruto'];
                $data->sub_total_price = $val['sub_total_price'];
                $data->created_at = date("Y-m-d H:i:s"); //this created_at is a magic method you can ignore this
                $data->updated_at = date("Y-m-d H:i:s"); //this created_at is a magic method you can ignore this
                $data->save();
            }
        }

        return PurchaseDetail::findAllBy("purchase_id",$purchase_id);
    }

    public static function update($array,$purchase_id)
    {
        PurchaseDetail::table()->where('purchase_id',$purchase_id)->delete();

        foreach($array as $val)
        {
            if($val['sub_total_price'] != 0)
            {
                $data = new PurchaseDetail();
                $data->purchase_id = $purchase_id;
                $data->products_id = $val['products_id'];
                $data->products_name = $val['products_name'];
                $data->products_price = $val['products_price'];
                $data->netto = $val['netto'];
                $data->bruto = $val['bruto'];
                $data->sub_total_price = $val['sub_total_price'];
                $data->created_at = date("Y-m-d H:i:s"); //this created_at is a magic method you can ignore this
                $data->updated_at = date("Y-m-d H:i:s"); //this created_at is a magic method you can ignore this
                $data->save();
            }
        }

        return PurchaseDetail::findAllBy("purchase_id",$purchase_id);
    }
}