<?php
namespace App\Services;

use DB;
// use Illuminate\Support\Facades\DB;
use App\Repositories\Purchase;
use App\Repositories\Products;
use App\Repositories\SalesDetail;
use carbon\carbon;

class ProductsService extends Products
{
    public static function countAll()
    {
        return Products::table()->count();
    }

    public static function syncRetrieve($data)
    {
        $data = json_decode($data,true);

        Products::table()->delete();
        Products::table()->insert($data);   
    }

    public static function syncSend($type = null)
    {
        $data = Products::getAll();

        return sync("products",$data,$type);
    }
}