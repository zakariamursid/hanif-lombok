<?php
namespace App\Services;

use Illuminate\Support\Facades\DB;
use App\Repositories\Purchase;
use App\Repositories\PurchaseDetail;
use carbon\carbon;

class PurchaseService extends Purchase
{
    public static function countTodayTransaction()
    {
        return Purchase::table()->where('created_at','>=', Carbon::today()->toDateString())->count();
    }
    public static function sumTodayTransaction()
    {
        return idrFormat(Purchase::table()
        ->select(DB::raw('sum(price) as total'))
                ->where('created_at','>=', Carbon::today()->toDateString())
                ->first()
                ->total);
    }
    public static function sumTodayWeight()
    {
        $data = Purchase::table()
        ->select(DB::raw('sum(qty) as total'))
                ->where('created_at','>=', Carbon::today()->toDateString())
                ->first()
                ->total;

        if($data == null)
        {
            $data = 0;
        }

        return $data;
    }

    public static function insert($request)
    {
        $data = new Purchase();
        $data->name = $request['name'];
        $data->address = $request['address'];

        $total_price = 0;

        foreach($request['data'] as $val)
        {
            $total_price += $val['sub_total_price'];
        }

        if($total_price == 0)
        {
		    JSON::failed('Gagal menyimpan data, check kembali data product')->send();
            exit;
        }
        $data->status = $request['status'];
        $data->total_price = $total_price;
        $data->note = $request['note'];
        $data->created_at = date("Y-m-d H:i:s"); //this created_at is a magic method you can ignore this
        $data->updated_at = date("Y-m-d H:i:s"); //this created_at is a magic method you can ignore this
        $data->save();

        return $data;
    }

    public static function update($request)
    {
        $data = Purchase::findbyId($request['id']);

        if($data->id == NULL)
        {
            JSON::failed('Gagal update data')->send();
            exit;
        }

        $data->name = $request['name'];
        $data->address = $request['address'];

        $total_price = 0;

        foreach($request['data'] as $val)
        {
            $total_price += $val['sub_total_price'];
        }

        if($total_price == 0)
        {
		    JSON::failed('Gagal menyimpan data, check kembali data product')->send();
            exit;
        }
        $data->status = $request['status'];
        $data->total_price = $total_price;        
        
        if($request['note'] == null)
        {
            $note = ' ';
        }
        else
        {
            $note = $request['note'];
        }

        $data->note = $note;

        $data->created_at = date("Y-m-d H:i:s"); //this created_at is a magic method you can ignore this
        $data->updated_at = date("Y-m-d H:i:s"); //this created_at is a magic method you can ignore this
        $data->save();

        return $data;
    }

    public static function syncRetrieve($request)
    {
        $data = json_decode($request['data'],true);
        $type = $request['type'];
        $status = [];

        if($request['type'] == 'delete')
        {
            Purchase::deleteBy(["id" => $data['id']]);
            PurchaseDetail::deleteBy(["purchase_id" => $data['id']]);
        }
        else if($request['type'] == 'insert' || $request['type'] == 'update')
        {
            $details = $data['details'];
            unset($data['details']);
            $id = $data['id'];

            if($request['type'] == 'insert')
            {
                Purchase::deleteBy(["id" => $id]);
                $status['purchase'] = Purchase::table()->insert($data);
            }
            else
            {
                unset($data['id']);
                $status['purchase'] = Purchase::table()->where('id',$id)->update($data);
            }

            PurchaseDetail::deleteBy(["purchase_id" => $id]);
            $status['detail'] = PurchaseDetail::table()->insert($details);
        }
        return $status;

    }

    public static function syncSend($type = null,$data = null)
    {
        if($type == "insert" || $type == "update")
        {
            $data = json_encode($data,true);
        }
        else
        {
            $obj = new \stdClass();
            $obj->id = $data;
            $data = json_encode($obj,true);
        }

        return sync("purchase",$data,$type);
    }    

}