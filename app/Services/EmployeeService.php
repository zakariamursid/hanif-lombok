<?php
namespace App\Services;

use Illuminate\Support\Facades\DB;
use App\Repositories\Employee;
use carbon\carbon;

class EmployeeService extends Employee
{
    public static function insert($request)
    {
        $persons = [];

        foreach($request['data'] as $val)
        {
            $persons[] = $val;
        }   

        $data = new Employee();
        $data->persons = json_encode($persons);
        $data->date = Carbon::today()->toDateString();
        $data->updated_at = date("Y-m-d H:i:s");
        $data->save();

        return self::syncSend("insert",$data);
    }

    public static function update($request)
    {
        $persons = [];

        foreach($request['data'] as $val)
        {
            $persons[] = $val;
        }   

        $data = Employee::findById($request['id']);
        $data->persons = json_encode($persons);
        $data->updated_at = date("Y-m-d H:i:s");
        $data->save();

        return self::syncSend("update",$data);
    }

    public static function syncRetrieve($request)
    {
        $data = json_decode($request['data'],true);
        $type = $request['type'];
        $status = [];

        if($request['type'] == 'delete')
        {
            Employee::deleteBy(["id" => $data['id']]);
        }
        else if($request['type'] == 'insert' || $request['type'] == 'update')
        {
            $id = $data['id'];
            
            if($request['type'] == 'insert')
            {
                Employee::deleteBy(["id" => $id]);

                $status['employee'] = Employee::table()->insert($data);
            }
            else
            {
                unset($data['id']);
                $status['employee'] = Employee::table()->where('id',$id)->update($data);
            }
        }

        return $status;
    }

    public static function syncSend($type = null,$data = null)
    {
        if($type == "insert" || $type == "update")
        {
            $data = json_encode($data,true);
        }
        else
        {
            $obj = new \stdClass();
            $obj->id = $data;
            $data = json_encode($obj,true);
        }

        return sync("employee",$data,$type);
    }

}