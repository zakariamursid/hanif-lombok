<?php
namespace App\Services;

use Illuminate\Support\Facades\DB;
use App\Repositories\Sales;
use App\Repositories\SalesDetail;
use App\Helpers\JSON;
use carbon\carbon;

class SalesService extends Sales
{
    public static function countTodayTransaction()
    {
        return Sales::table()->where('created_at','>=', Carbon::today()->toDateString())->count();
    }
    public static function sumTodayTransaction()
    {
        return idrFormat(Sales::table()
        ->select(DB::raw('sum(total_price) as total'))
                ->where('created_at','>=', Carbon::today()->toDateString())
                ->first()
                ->total);
    }

    public static function insert($request)
    {
        $data = new Sales();
        $data->category = $request['category'];
        $data->name = $request['name'];
        $data->address = $request['address'];
        $data->note = $request['note'];
        $data->expedition = $request['expedition'];

        $total_price = 0;
        $total = 0;

        foreach($request['data'] as $val)
        {
            $total += (float)$val['sub_total'];
            $total_price += (float)$val['sub_total'] * (float)$val['products_price'];
        }

        if($total_price == 0)
        {
		    JSON::failed('Gagal menyimpan data, check kembali data product')->send();
            exit;
        }

        $data->total_price = $total_price;
        $data->total = $total;
        $data->created_at = date("Y-m-d H:i:s"); //this created_at is a magic method you can ignore this
        $data->updated_at = date("Y-m-d H:i:s"); //this created_at is a magic method you can ignore this

        $data->save();

        return $data;
    }

    public static function update($request)
    {
        $data = Sales::findbyId($request['id']);

        if($data->id == NULL)
        {
            JSON::failed('Gagal update data')->send();
            exit;
        }

        $data->category = $request['category'];
        $data->name = $request['name'];
        $data->address = $request['address'];
        $data->expedition = $request['expedition'];

        if($request['note'] == null)
        {
            $note = ' ';
        }
        else
        {
            $note = $request['note'];
        }

        $data->note = $note;

        $total_price = 0;
        $total = 0;

        foreach($request['data'] as $val)
        {
            $total += (float)$val['sub_total'];
            $total_price += (float)$val['sub_total'] * (float)$val['products_price'];
        }

        if($total_price == 0)
        {
		    JSON::failed('Gagal menyimpan data, check kembali data product')->send();
            exit;
        }

        $data->total_price = $total_price;
        $data->total = $total;
        $data->created_at = date("Y-m-d H:i:s"); //this created_at is a magic method you can ignore this
        $data->updated_at = date("Y-m-d H:i:s"); //this created_at is a magic method you can ignore this
        $data->save();

        return $data;
    }

    public static function syncRetrieve($request)
    {
        $data = json_decode($request['data'],true);
        $type = $request['type'];
        $status = [];
        
        if($request['type'] == 'delete')
        {
            Sales::deleteBy(["id" => $data['id']]);
            SalesDetail::deleteBy(["sales_id" => $data['id']]);
        }
        else if($request['type'] == 'insert' || $request['type'] == 'update')
        {
            $details = $data['details'];
            unset($data['details']);
            $id = $data['id'];
            
            if($request['type'] == 'insert')
            {
                Sales::deleteBy(["id" => $id]);

                $status['sales'] = Sales::table()->insert($data);
            }
            else
            {
                unset($data['id']);
                $status['sales'] = Sales::table()->where('id',$id)->update($data);
            }

            SalesDetail::deleteBy(["sales_id" => $id]);
            $status['detail'] = SalesDetail::table()->insert($details);
            return $status;
        }

    }

    public static function syncSend($type = null,$data = null)
    {
        if($type == "insert" || $type == "update")
        {
            $data = json_encode($data,true);
        }
        else
        {
            $obj = new \stdClass();
            $obj->id = $data;
            $data = json_encode($obj,true);
        }

        return sync("sales",$data,$type);
    }

}