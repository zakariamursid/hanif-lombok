<?php
namespace App\Models;

use Crocodic\LaravelModel\Core\Model;

class NotesModel extends Model
{
    
	public $id;
	public $date;
	public $content;
	public $created_at;
	public $updated_at;

}