<?php
namespace App\Models;

use Crocodic\LaravelModel\Core\Model;

class SalesDetailModel extends Model
{
    
	public $id;
	public $sales_id;
	public $products_name;
	public $created_at;
	public $updated_at;
	public $products_id;
	public $products_price;
	public $sub_total_price;
	public $pack;
	public $qty;
	public $sub_total;
	public $purchase_price;
	public $koli;
	public $koli_total;

}