<?php
namespace App\Models;

use Crocodic\LaravelModel\Core\Model;

class PurchaseDetailModel extends Model
{
    
	public $id;
	public $products_name;
	public $products_price;
	public $netto;
	public $bruto;
	public $sub_total_price;
	public $created_at;
	public $updated_at;
	public $products_id;
	public $purchase_id;

}