<?php
namespace App\Models;

use Crocodic\LaravelModel\Core\Model;

class ProductsModel extends Model
{
    
	public $id;
	public $category_id;
	public $name;
	public $price;
	public $created_at;
	public $updated_at;
	public $sort;

}