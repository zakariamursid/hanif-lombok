<?php
namespace App\Models;

use Crocodic\LaravelModel\Core\Model;

class PurchaseModel extends Model
{
    
	public $id;
	public $note;
	public $created_at;
	public $updated_at;
	public $name;
	public $address;
	public $total_price;
	public $status;

}