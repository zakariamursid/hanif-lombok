<?php
namespace App\Models;

use Crocodic\LaravelModel\Core\Model;

class SalesModel extends Model
{
    
	public $id;
	public $name;
	public $created_at;
	public $updated_at;
	public $address;
	public $note;
	public $expedition;
	public $total_price;
	public $category;
	public $total;

}