<?php
namespace App\Models;

use Crocodic\LaravelModel\Core\Model;

class CategoryModel extends Model
{
    
	public $id;
	public $name;
	public $created_at;
	public $updated_at;

}