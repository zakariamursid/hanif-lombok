<?php
namespace App\Repositories;
use App\Repositories\SalesDetail;
use App\Models\SalesModel;

class Sales extends SalesModel
{
    public static function getById($id)
    {
        $data = Sales::findById($id);

        if($data->id != null)
        {
            $data->details = SalesDetail::findAllBy("sales_id",$id);
        }

        return $data;
    }

    public static function getByDate($date)
    {
        $data = [];
        $id = [];

        if($date)
        {
            $data = Sales::table()->whereDate('created_at',$date)->get();

            if(count($data) > 0)
            {
                foreach($data as $val)
                {
                    $id[] = $val->id;
                }

                $details = SalesDetail::getWhereIn('sales_id',$id);
                $details = collect($details)->groupBy('sales_id')->toArray();

                foreach($data as $val)
                {
                    if(array_key_exists($val->id, $details))
                    {
                        $val->details = $details[$val->id];
                    }
                    else
                    {
                        $val->details = [];
                    }
                }
            }
        }

        return $data;
    }
}