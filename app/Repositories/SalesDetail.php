<?php
namespace App\Repositories;

use App\Models\SalesDetailModel;

class SalesDetail extends SalesDetailModel
{
    public static function getWhereIn($key,$val)
    {
        return SalesDetail::table()->whereIn($key,$val)->get();
    }
}