<?php
namespace App\Repositories;
use App\Models\PurchaseDetailModel;
use carbon\carbon;
use DB;

class PurchaseDetail extends PurchaseDetailModel
{
    public static function getWhereIn($key,$val)
    {
        return PurchaseDetail::table()->whereIn($key,$val)->get();
    }

    public static function getTodayByProductsId($products_id)
    {        
        $data = PurchaseDetail::table()
        ->select(DB::raw('sum(netto) as total'))
        ->where('created_at','>=', Carbon::today()->toDateString())
        ->where('products_id',$products_id)
        ->first()
        ->total;

        if($data == null)
        {
            $data = 0;
        }

        return $data;
    }
}