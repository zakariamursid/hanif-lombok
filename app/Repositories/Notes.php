<?php
namespace App\Repositories;

use App\Models\NotesModel;

class Notes extends NotesModel
{
    // TODO : Make your own query methods
    public static function getByDate($date)
    {
        return Notes::table()->where('date',$date)->orderBy('id','desc')->first();
    }
}