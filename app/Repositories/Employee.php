<?php
namespace App\Repositories;

use App\Models\EmployeeModel;
use carbon\carbon;

class Employee extends EmployeeModel
{
    public static function getToday()
    {
        return Employee::table()->where('date',Carbon::today()->toDateString())->first();
    }
    public static function getByDate($date)
    {
        return Employee::table()->where('date',$date)->first();
    }

    public static function getById($id)
    {
        return Employee::table()->where('id',$id)->first();
    }
}