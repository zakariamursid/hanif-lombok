<?php
namespace App\Repositories;

use App\Models\PurchaseModel;
use App\Repositories\PurchaseDetail;

class Purchase extends PurchaseModel
{
    // TODO : Make your own query methods
    public static function getById($id)
    {
        $data = Purchase::findById($id);

        if($data->id != null)
        {
            $data->details = PurchaseDetail::findAllBy("purchase_id",$data->id);
        }

        return $data;
    }

    public static function getByDate($date)
    {
        $data = [];
        $id = [];

        if($date)
        {
            $data = Purchase::table()->whereDate('created_at',$date)->get();
            
            foreach($data as $val)
            {
                $id[] = $val->id;
            }

            if(count($data) > 0)
            {
                $details = PurchaseDetail::getWhereIn('purchase_id',$id);

                $details = collect($details)->groupBy('purchase_id')->toArray();

                foreach($data as $val)
                {
                    if(array_key_exists($val->id, $details))
                    {
                        $val->details = $details[$val->id];
                    }
                    else
                    {
                        $val->details = [];
                    }
                }
            }
        }

        return $data;
    }
}