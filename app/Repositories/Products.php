<?php
namespace App\Repositories;

use App\Models\ProductsModel;

class Products extends ProductsModel
{
    public static function getAll()
    {
        return Products::table()->orderBy('sort','asc')->get();
    }

    public static function getMax()
    {
        return Products::table()->max('sort');
    }

    public static function setSort()
    {
        return self::getMax() + 1;
    }

}