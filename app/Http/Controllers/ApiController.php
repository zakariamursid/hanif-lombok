<?php

namespace App\Http\Controllers;
use DB;
use App\Services\SalesService;
use App\Services\ProductsService;
use App\Services\PurchaseService;
use App\Services\EmployeeService;
use Illuminate\Http\Request;
use Validator;
use App\Helpers\JSON;

class ApiController extends Controller
{
    public function postSync(Request $request)
    {
        $form = array
        (
            'module'	=> 'required',
            'data'	=> 'required',
        );		

        $rules = array
        (
            'module.required' => 'Module harus diisi',
            'data.required' => 'Data harus diisi',
        );

        $validator = Validator::make(request()->all(),$form,$rules);    
        validateJSON($validator); 
        DB::beginTransaction();

        try
        {
            $status = [];

            if(request()->module == 'products')
            {
                $status = ProductsService::syncRetrieve(request()->data);   
            }
            else if(request()->module == 'employee')
            {
                $status = EmployeeService::syncRetrieve(request()->all());  
            }
            else if(request()->module == 'sales')
            {
                $status = SalesService::syncRetrieve(request()->all());  
            }
            else if(request()->module == 'purchase')
            {
                $status = PurchaseService::syncRetrieve(request()->all()); 
            }

            $response = [];

            $response['status'] = 200;
            $response['type'] = 'success';
            $response['message'] = 'Berhasil update data';
            $response['data'] = request()->module;
            $response['data_status'] = $status;

            DB::commit();

            return response()->json($response,200);

        }
        catch (\Throwable $e)
        {
            DB::rollback();

            return JSON::failed($e->getMessage());
        }      
    }
}