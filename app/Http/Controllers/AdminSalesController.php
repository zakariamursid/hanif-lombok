<?php namespace App\Http\Controllers;

	use Session;
	use Request;
	use DB;
	use CRUDBooster;
	use App\Repositories\Products;
	use App\Repositories\Sales;
	use App\Repositories\SalesDetail;
	use Validator;
	use App\Helpers\JSON;
	use App\Services\SalesService;
	use App\Services\SalesDetailService;
	use charlieuki\ReceiptPrinter\ReceiptPrinterSales as ReceiptPrinter;

	class AdminSalesController extends \crocodicstudio\crudbooster\controllers\CBController {

	    public function cbInit() {

			# START CONFIGURATION DO NOT REMOVE THIS LINE
			$this->title_field = "name";
			$this->limit = "20";
			$this->orderby = "id,desc";
			$this->global_privilege = false;
			$this->button_table_action = true;
			$this->button_bulk_action = true;
			$this->button_action_style = "button_icon";
			$this->button_add = true;
			$this->button_edit = true;
			$this->button_delete = true;
			$this->button_detail = true;
			$this->button_show = true;
			$this->button_filter = true;
			$this->button_import = false;
			$this->button_export = false;
			$this->table = "sales";
			# END CONFIGURATION DO NOT REMOVE THIS LINE

			# START COLUMNS DO NOT REMOVE THIS LINE
			$this->col = [];
			$this->col[] = ["label"=>"Kategori","name"=>"category"];
			$this->col[] = ["label"=>"Nama Pembeli","name"=>"name"];
			$this->col[] = ["label"=>"Alamat","name"=>"address"];
			$this->col[] = ["label"=>"Catatan","name"=>"note"];
			$this->col[] = array("label"=>"Terakhir diupdate","name"=>"updated_at","callback"=>function($row)
			{
				return dateTimeFormat($row->updated_at);
			});
			$this->col[] = array("label"=>"Opsi","name"=>"id","callback"=>function($row)
			{
				$url = CRUDBooster::mainpath('print/'.$row->id);

				$html = "
				<a href='". CRUDBooster::mainpath('print/'.$row->id)."'>
				<div class='btn-group'>
				
					<button type='button' class='btn btn-xs btn-danger' aria-haspopup='true' aria-expanded='false'>
						Cetak
					</button>
				</div>
				</a>";

				return $html;
			});
			# END COLUMNS DO NOT REMOVE THIS LINE

			# START FORM DO NOT REMOVE THIS LINE
			$this->form = [];
			$this->form[] = ['label'=>'Nama Pembeli','name'=>'name','type'=>'text','validation'=>'required|string|min:3|max:70','width'=>'col-sm-10','placeholder'=>'Anda hanya dapat memasukkan huruf saja'];
			$this->form[] = ['label'=>'Alamat','name'=>'address','type'=>'text','validation'=>'required|string|min:3|max:70','width'=>'col-sm-10','placeholder'=>'Anda hanya dapat memasukkan huruf saja'];
			$this->form[] = ['label'=>'Catatan','name'=>'note','type'=>'text','validation'=>'required|string|min:3|max:70','width'=>'col-sm-10','placeholder'=>'Anda hanya dapat memasukkan huruf saja'];
			$this->form[] = ['label'=>'Total Harga','name'=>'money','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			# END FORM DO NOT REMOVE THIS LINE

			# OLD START FORM
			//$this->form = [];
			//$this->form[] = ["label"=>"Name","name"=>"name","type"=>"text","required"=>TRUE,"validation"=>"required|string|min:3|max:70","placeholder"=>"Anda hanya dapat memasukkan huruf saja"];
			//$this->form[] = ["label"=>"Total Price","name"=>"total_price","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
			# OLD END FORM

			/* 
	        | ---------------------------------------------------------------------- 
	        | Sub Module
	        | ----------------------------------------------------------------------     
			| @label          = Label of action 
			| @path           = Path of sub module
			| @foreign_key 	  = foreign key of sub table/module
			| @button_color   = Bootstrap Class (primary,success,warning,danger)
			| @button_icon    = Font Awesome Class  
			| @parent_columns = Sparate with comma, e.g : name,created_at
	        | 
	        */
	        $this->sub_module = array();


	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add More Action Button / Menu
	        | ----------------------------------------------------------------------     
	        | @label       = Label of action 
	        | @url         = Target URL, you can use field alias. e.g : [id], [name], [title], etc
	        | @icon        = Font awesome class icon. e.g : fa fa-bars
	        | @color 	   = Default is primary. (primary, warning, succecss, info)     
	        | @showIf 	   = If condition when action show. Use field alias. e.g : [id] == 1
	        | 
	        */
	        $this->addaction = array();


	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add More Button Selected
	        | ----------------------------------------------------------------------     
	        | @label       = Label of action 
	        | @icon 	   = Icon from fontawesome
	        | @name 	   = Name of button 
	        | Then about the action, you should code at actionButtonSelected method 
	        | 
	        */
	        $this->button_selected = array();

	                
	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add alert message to this module at overheader
	        | ----------------------------------------------------------------------     
	        | @message = Text of message 
	        | @type    = warning,success,danger,info        
	        | 
	        */
	        $this->alert        = array();
	                

	        
	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add more button to header button 
	        | ----------------------------------------------------------------------     
	        | @label = Name of button 
	        | @url   = URL Target
	        | @icon  = Icon from Awesome.
	        | 
	        */
	        $this->index_button = array();



	        /* 
	        | ---------------------------------------------------------------------- 
	        | Customize Table Row Color
	        | ----------------------------------------------------------------------     
	        | @condition = If condition. You may use field alias. E.g : [id] == 1
	        | @color = Default is none. You can use bootstrap success,info,warning,danger,primary.        
	        | 
	        */
	        $this->table_row_color = array();     	          

	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | You may use this bellow array to add statistic at dashboard 
	        | ---------------------------------------------------------------------- 
	        | @label, @count, @icon, @color 
	        |
	        */
	        $this->index_statistic = array();
			// $this->index_statistic[] = ['label'=>'Transaksi','count'=>SalesService::countTodayTransaction(),'icon'=>'fa fa-exchange','color'=>'success'];
			// $this->index_statistic[] = ['label'=>'Uang Masuk','count'=>SalesService::sumTodayTransaction(),'icon'=>'fa fa-check','color'=>'info'];


	        /*
	        | ---------------------------------------------------------------------- 
	        | Add javascript at body 
	        | ---------------------------------------------------------------------- 
	        | javascript code in the variable 
	        | $this->script_js = "function() { ... }";
	        |
	        */
	        $this->script_js = NULL;


            /*
	        | ---------------------------------------------------------------------- 
	        | Include HTML Code before index table 
	        | ---------------------------------------------------------------------- 
	        | html code to display it before index table
	        | $this->pre_index_html = "<p>test</p>";
	        |
	        */
	        $this->pre_index_html = null;
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include HTML Code after index table 
	        | ---------------------------------------------------------------------- 
	        | html code to display it after index table
	        | $this->post_index_html = "<p>test</p>";
	        |
	        */
	        $this->post_index_html = null;
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include Javascript File 
	        | ---------------------------------------------------------------------- 
	        | URL of your javascript each array 
	        | $this->load_js[] = asset("myfile.js");
	        |
	        */
	        $this->load_js = array();
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Add css style at body 
	        | ---------------------------------------------------------------------- 
	        | css code in the variable 
	        | $this->style_css = ".style{....}";
	        |
	        */
	        $this->style_css = NULL;
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include css File 
	        | ---------------------------------------------------------------------- 
	        | URL of your css each array 
	        | $this->load_css[] = asset("myfile.css");
	        |
	        */
	        $this->load_css = array();
	        
	        
	    }


	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for button selected
	    | ---------------------------------------------------------------------- 
	    | @id_selected = the id selected
	    | @button_name = the name of button
	    |
	    */
	    public function actionButtonSelected($id_selected,$button_name) {
	        //Your code here
	            
	    }


	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate query of index result 
	    | ---------------------------------------------------------------------- 
	    | @query = current sql query 
	    |
	    */
	    public function hook_query_index(&$query) {
	        //Your code here
	            
	    }

	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate row of index table html 
	    | ---------------------------------------------------------------------- 
	    |
	    */    
	    public function hook_row_index($column_index,&$column_value) {	        
	    	//Your code here
	    }

	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate data input before add data is execute
	    | ---------------------------------------------------------------------- 
	    | @arr
	    |
	    */
	    public function hook_before_add(&$postdata) {        
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after add public static function called 
	    | ---------------------------------------------------------------------- 
	    | @id = last insert id
	    | 
	    */
	    public function hook_after_add($id) {        
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate data input before update data is execute
	    | ---------------------------------------------------------------------- 
	    | @postdata = input post data 
	    | @id       = current id 
	    | 
	    */
	    public function hook_before_edit(&$postdata,$id) {        
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after edit public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_after_edit($id) {
	        //Your code here 

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command before delete public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_before_delete($id) {
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after delete public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_after_delete($id) {
	        //Your code here
            SalesDetail::deleteBy(["sales_id" => $id]);
			SalesService::syncSend('delete',$id);   
	    }



	    //By the way, you can still create your own method in here... :) 
		public function getAdd()
		{
			//Create an Auth
			if(!CRUDBooster::isCreate() && $this->global_privilege==FALSE || $this->button_add==FALSE) {    
				CRUDBooster::redirect(CRUDBooster::adminPath(),trans("crudbooster.denied_access"));
			}

			$data = [];
			$data['url'] = '/insert';
			$data['page_title'] = "Tambah Barang Keluar";
			$data['products'] = Products::getAll();
			$data['category'] = array('Lokal','Sumatera','Pabrik');
			$data['row'] = [];
			$data['details'] = [];

			return view('admin.sales.form',$data);
		}

		public function getEdit($id)
		{
			//Create an Auth
			if(!CRUDBooster::isCreate() && $this->global_privilege==FALSE || $this->button_add==FALSE) {    
				CRUDBooster::redirect(CRUDBooster::adminPath(),trans("crudbooster.denied_access"));
			}

			$data = [];
			$data['url'] = '/update';
			$data['page_title'] = "Edit Barang Keluar";
			$data['products'] = Products::getAll();
			$data['category'] = array('Lokal','Sumatera','Pabrik');
			$data['row'] = Sales::getById($id);

			$details = [];

			if($data['row'])
			{
				foreach($data['row']->details as $detail)
				{
					$details[$detail->products_id] = $detail;
				}
			}

			$data['details'] = $details;

			return view('admin.sales.form',$data);
		}

		public function getDetail($id)
		{
			//Create an Auth
			if(!CRUDBooster::isCreate() && $this->global_privilege==FALSE || $this->button_add==FALSE) {    
				CRUDBooster::redirect(CRUDBooster::adminPath(),trans("crudbooster.denied_access"));
			}

			$data = [];
			$data['page_title'] = "Detail Barang Keluar";
			$data['row'] = Sales::getById($id);

			return view('admin.sales.detail',$data);
		}

		public function postInsert()
		{
			//Create an Auth
			if(!CRUDBooster::isCreate() && $this->global_privilege==FALSE || $this->button_add==FALSE) {    
				CRUDBooster::redirect(CRUDBooster::adminPath(),trans("crudbooster.denied_access"));
			}

			$form = array
            (
				'name'	=> 'required',
				'address'	=> 'required',
				'data'	=> 'required',
            );		

			$rules = array
            (
                'name.required' => 'Nama pembeli harus diisi',
                'address.required' => 'alamat harus diisi',
                'data.required' => 'Data harus diisi',
			);

			$validator = Validator::make(request()->all(),$form,$rules);    
			validateJSON($validator); 
			DB::beginTransaction();

			try
			{
				$data = SalesService::insert(request()->all());
				$data->details = SalesDetailService::insert(request()->data,$data->id);

				DB::commit();

				$res = SalesService::syncSend('insert',$data);   
				$res['id'] = $data->id;

				if($res['status'] == 200)
				{
					return response()->json($res,200);
				}
				else
				{
					return response()->json($res,500);
				}

			}
			catch (\Throwable $e)
			{
				DB::rollback();
	
				return JSON::failed($e->getMessage());
			}      
		}

		public function postUpdate()
		{
			//Create an Auth
			if(!CRUDBooster::isCreate() && $this->global_privilege==FALSE || $this->button_add==FALSE) {    
				CRUDBooster::redirect(CRUDBooster::adminPath(),trans("crudbooster.denied_access"));
			}

			$form = array
            (
				'id'	=> 'required',
				'name'	=> 'required',
				'address'	=> 'required',
				'data'	=> 'required',
            );		

			$rules = array
            (
                'id.required' => 'ID harus diisi',
                'name.required' => 'Nama pembeli harus diisi',
                'address.required' => 'alamat harus diisi',
                'data.required' => 'Data harus diisi',
			);

			$validator = Validator::make(request()->all(),$form,$rules);    
			validateJSON($validator); 
			DB::beginTransaction();

			try
			{
				$data = SalesService::update(request()->all());
				$data->details = SalesDetailService::update(request()->data,$data->id);

				$response = [];
				DB::commit();

				$res = SalesService::syncSend('update',$data);  
				$res['id'] = $data->id;
				
				if($res['status'] == 200)
				{
					return response()->json($res,200);
				}
				else
				{
					return response()->json($res,500);
				}
			}
			catch (\Throwable $e)
			{
				DB::rollback();
	
				return JSON::failed($e->getMessage());
			}      
		}

		public function getPrint($id)
		{
			//Create an Auth
			if(!CRUDBooster::isCreate() && $this->global_privilege==FALSE || $this->button_add==FALSE) {    
				CRUDBooster::redirect(CRUDBooster::adminPath(),trans("crudbooster.denied_access"));
			}

			if(getHost() == getLocalhost())
			{

				$data = Sales::getById($id);
				// dd($data);

				// Set params
				$mid = $data->note;
				$store_name = "KARYA CABE";
				$store_address = "-----";
				$store_phone = '1234567890';
				$store_email = 'yourmart@email.com';
				$store_website = 'yourmart.com';
				$tax_percentage = 0;
				$transaction_id = $data->name .'-' .$data->address;


				$items = [];

				foreach($data->details as $it)
				{
					$obj = array();
					$obj['name'] = $it->products_name;
					$obj['qty'] = $it->qty;
					$obj['sales_price'] = $it->products_price;
					$obj['purchase_price'] = $it->purchase_price;
					// $obj['qty'] = $it->netto .'/' .$it->bruto;
					$obj['price'] = $it->pack;
					$obj['koli'] = $it->koli;
					$obj['koli_total'] = $it->koli_total;
					$obj['sub_total'] = $it->sub_total;
				
					$items[] = $obj;
				}

				// Init printer
				$printer = new ReceiptPrinter;
				$printer->init(
					config('receiptprinter.connector_type'),
					config('receiptprinter.connector_descriptor')
				);

				$total_price = 0;

				// Set store info
				$printer->setStore($mid, $store_name, $store_address, $store_phone, $store_email, $store_website);
				// Add items
				foreach ($items as $item) {
					$printer->addItem(
						$item['name'],
						$item['qty'],
						$item['price'],
						$item['purchase_price'],
						$item['sales_price'],
						$item['koli'],
						$item['koli_total'],
						$item['sub_total']
					);

					if($item['purchase_price'] > 0)
					{
						$total_price += 0;
					}
					else
					{
						$total_price += $item['sales_price'] * $item['sub_total'];
						// dd($item);
					}

    //         if($item->getPurchasePrice() == 0)
    //         {
    //             $this->totalprice += (int) $item->getQty() * (int) $item->getPrice() * (int) $item->getSalesPrice();
    //         }
    //         else
    //         {
    //             $this->totalprice = 0;
    //         }

				}

				// Calculate total
				// $printer->calculateGrandTotalPrice();
				$printer->calculateSubTotal();
				$printer->calculateGrandTotal();
				$printer->setGrandTotal($data->total);
				$printer->setGrandTotalPrice($total_price);
				$printer->setDate(dayTimeFormat(date("Y-m-d H:i:s")));

				// Set transaction ID
				$printer->setTransactionID($transaction_id);

				// Set qr code
				$printer->setQRcode([
					'tid' => $transaction_id,
				]);

				// Print receipt
				$printer->printReceipt();

				return redirect(Request::server('HTTP_REFERER'))->with([
					'message'=>'Data berhasil disimpan dan sudah di cetak',
					'message_type'=>'success'
				]);
			}
			else
			{
				return redirect(Request::server('HTTP_REFERER'))->with([
					'message'=>'Printer tidak terhubung',
					'message_type'=>'danger'
				]);		
			}
			// return redirect(Request::server('HTTP_REFERER'))->with([
			// 	'message'=>'Berhasil di print',
			// 	'message_type'=>'success'
			// ]);
		}
	}