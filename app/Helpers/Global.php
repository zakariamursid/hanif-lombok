<?php
use Illuminate\Support\Facades\Validator;

if (! function_exists('validateJSON'))
{
    function validateJSON($validator)
    {
        $result = [];
        if ($validator->fails()) 
        {
            $message = $validator->errors()->all();      
            $result['status'] = 400;
            $result['message'] = implode(', ',$message);      
            $res = response()->json($result);
            $res->send();
            exit;
        }    
    }
}

if (! function_exists('idrFormat'))
{
    function idrFormat($nominal)
    {
        $value = "Rp. " . number_format($nominal,0,',','.');;
        return $value;
    }
}    

if (! function_exists('dayTimeFormat'))
{
    function dayTimeFormat($date)
    {
        if($date != NULL)
        {
            $formated_date =  Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $date, 'Asia/Jakarta')
            ->setTimezone('Asia/Jakarta')->isoFormat('dddd, D MMM Y');
    
            $time = date('H:i:s', strtotime($date));
            
            $date = $formated_date ." " .$time;
            return $date;
        }
    }
}

if (! function_exists('dateTimeFormat'))
{
    function dateTimeFormat($date)
    {
        if($date != NULL)
        {
            $formated_date =  Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $date, 'Asia/Jakarta')
            ->setTimezone('Asia/Jakarta')->isoFormat('DD-MM-YY');
            // ->setTimezone('Asia/Jakarta')->isoFormat('dddd, D MMM Y');
    
            $time = date('H:i:s', strtotime($date));
            
            $date = $formated_date ." " .$time;
            return $date;
        }
    }
}  

if (! function_exists('dateFormat'))
{
    function dateFormat($date)
    {
        if($date != NULL)
        {
            $formated_date =  Carbon\Carbon::createFromFormat('Y-m-d', $date, 'Asia/Jakarta')
            ->setTimezone('Asia/Jakarta')->isoFormat('DD MMMM YYYY');
            
            return $formated_date;
        }
    }
}  

if (! function_exists('retrieve_json'))
{
    function retrieve_json($json)
    {
        if($json != NULL)
        {
            $json = json_decode($json);

            $final = implode(", ", $json);

            return $final;
        }
    }
}  

if (! function_exists('getHost'))
{
    function getHost()
    {
        return request()->getHttpHost();
    }
}  

if (! function_exists('getLocalhost'))
{
    function getLocalhost()
    {
        return "localhost:8888";
    }
}  

if (! function_exists('sync'))
{
    function sync($module,$data,$type = null)
    {
        if(getHost() == getLocalhost())
        {

            $curl = curl_init();
            
            curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://neptune74.crocodic.net/jaka/public/api/sync',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => array('module' => $module,'data' => $data,'type' => $type),
            ));
            
            $response = curl_exec($curl);
            curl_close($curl);
            
            if($response != 'Not Found' || $respons == false)
            {

                $res = [];
                $res['status'] = 200;
                $res['type'] = 'success';
                $res['message'] = 'OK';

                return $res;
            }
            else
            {
                return json_decode($response,true);
            }
        }
        else
        {
            $res = [];
            $res['status'] = 200;
            $res['type'] = 'success';
            $res['message'] = 'OK';

            return $res;
        }

    }
}  
