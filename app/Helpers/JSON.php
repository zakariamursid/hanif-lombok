<?php
namespace App\Helpers;

class JSON
{
    public static function success($message = 'OK')
    {
        $response = [];

        $response['status'] = 200;
        $response['type'] = 'success';
        $response['message'] = $message;

        return response()->json($response,200);
    }

    public static function failed($message = 'OK',$status = 500)
    {
        $response = [];

        $response['status'] = 500;
        $response['type'] = 'failed';
        $response['message'] = $message;

        return response()->json($response,500);
    }
}