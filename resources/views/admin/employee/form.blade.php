<!-- First, extends to the CRUDBooster Layout -->
@extends('crudbooster::admin_template')
@section('content')

<p><a title="Return" href="{{CrudBooster::mainpath('')}}"><i class="fa fa-chevron-circle-left "></i>
                        &nbsp; Kembali ke halaman Barang Keluar</a></p>

  <!-- Your html goes here -->
  <div class='panel panel-default'>
    <div class='panel-heading'>{{$page_title}}</div>
    <div class='panel-body'>
      <form method='post' id="form">
        <div style="padding:10px;">
            <input type="hidden" class="form-control" name="id" id="id" value="{{$row->id}}">
            <table class="table" id="employee_table">
                <tbody>
                @if(count($persons) > 0)
                    @foreach($persons as $key => $val)
                        <tr>
                            <td width="20%"><input type="text" name="data[{{$key}}]" placeholder="Nama" class="form-control" value="{{$val}}"/></td>
                            <td width="10%">
                                <button type="button" name="add" id="add" class="btn btn-sm btn-success"><i class="fa fa-plus"></i></button>&nbsp;
                                @if($key > 0)
                                    <button type="button" name="remove" id="remove" class="btn btn-sm btn-danger remove"><i class="fa fa-trash"></i></button>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>

        <button type="button" id="btnSave" class="btn btn-sm btn-info pull-right" onclick="confirm('{{$url}}')">Simpan</button>

      </form>
    </div>
  </div>
@endsection
@push('bottom')
<script>
    let mainpath = "{{ CRUDBooster::mainpath('') }}";
    let count = {{$count}};
</script>
<script src="{{asset('assets/js/admin/employee.js')}}"></script>
<script>
    $(document).ready(function()
    {
        if(count < 1)
        {
            dynamic_field(1);
        }
        else
        {
            dynamic_field(count);
        }
    });

    const confirm = (action) =>
    {
        swal({
            title: "Apakah anda yakin?",
            text: "",
            icon: "warning",
            buttons: true,
            dangerMode: true,
            showCancelButton: true,
            confirmButtonText: "Ya",
            cancelButtonText: "Batal",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function(isConfirm)
        {
            if (isConfirm)
            {
                insert(action);
            }
            else
            {
                swal("Dibatalkan", "", "error"); 
            }
        });
    }
</script>

@endpush