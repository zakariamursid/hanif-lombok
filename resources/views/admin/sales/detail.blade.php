<!-- First, extends to the CRUDBooster Layout -->
@extends('crudbooster::admin_template')
@section('content')

<p><a title="Return" href="{{CrudBooster::mainpath('')}}"><i class="fa fa-chevron-circle-left "></i>
                        &nbsp; Kembali ke halaman Barang Keluar</a></p>

  <!-- Your html goes here -->
  <div class='panel panel-default'>
    <div class='panel-heading'>{{$page_title}}</div>
    <div class='panel-body'>
        <div style="padding:10px;">
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Kategori</label>
                <div class="col-sm-10">
                    {{$row->category}}
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Nama Pembeli</label>
                <div class="col-sm-10">
                    {{$row->name}}
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Alamat</label>
                <div class="col-sm-10">
                    {{$row->address}}
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Catatan</label>
                <div class="col-sm-10">
                    {{$row->note}}
                </div>
            </div>
            @if($row->category == "Sumatera")
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Ekspedisi</label>
                <div class="col-sm-10">
                    {{$row->expedition}}
                </div>
            </div>
            @endif

            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Diinput Pada</label>
                <div class="col-sm-10">
                    {{dateTimeFormat($row->created_at)}}
                </div>
            </div>
        </div>

            <table class="table">
            <thead>
                <tr>
                    <th>Nama Produk</th>
                    <th>Belanja</th>
                    <th>Harga</th>
                    <th>Kotak</th>
                    <th>Jumlah</th>
                    <th>Koli</th>
                    <th>Total Koli</th>
                    <th>Sub Total</th>
                </tr>
            </thead>
            <tbody>
                @if(count($row->details) > 0)
                    @foreach($row->details as $key => $val)
                    <tr>
                        <td width="15%">{{$val->products_name}}</td>
                        <td width="10%">
                        @if($val->purchase_price)
                        {{idrFormat($val->purchase_price)}}
                        @else
                        -
                        @endif

                        </td>
                        <td width="10%">{{idrFormat($val->products_price)}}</td>
                        <td width="10%">{{$val->pack}}</td>
                        <td width="10%">{{$val->qty}} KG</td>
                        <td width="10%">
                        @if($val->koli_total)
                        {{$val->koli}} 
                        @else
                        -
                        @endif
                        </td>
                        <td width="10%">
                        @if($val->koli_total)
                        {{$val->koli_total}} 
                        KG
                        @else
                        -
                        @endif
                        </td>
                        <td width="20%">{{$val->sub_total}} KG</td>
                    </tr>
                    @endforeach
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>
                            {{$row->total}} KG
                        </td>
                     </tr>
                @endif
            </tbody>
        </table>

        <a onclick="confirm('Apakah anda yakin?')" class="btn btn-sm btn-info" href="{{CRUDBooster::mainpath('print/'.$row->id)}}">PRINT</a>
    </div>
  </div>
@endsection
