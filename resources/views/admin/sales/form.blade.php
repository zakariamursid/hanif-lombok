<!-- First, extends to the CRUDBooster Layout -->
@extends('crudbooster::admin_template')
@section('content')

<p><a title="Return" href="{{CrudBooster::mainpath('')}}"><i class="fa fa-chevron-circle-left "></i>
                        &nbsp; Kembali ke halaman Barang Keluar</a></p>

  <!-- Your html goes here -->
  <div class='panel panel-default'>
    <div class='panel-heading'>{{$page_title}}</div>
    <div class='panel-body'>
      <form method='post' id="form">
        <div style="padding:10px;">
            <input type="hidden" class="form-control" name="id" id="id" value="{{$row->id}}">
            <div class="form-group row">
                <label for="inputCategory" class="col-sm-2 col-form-label">Kategori</label>
                <div class="col-sm-10">
                    <select name="category" id="category" class="form-control">
                        @foreach($category as $cat)
                            <option @if(isset($row)) @if($row->category == $cat) selected @endif @endif value="{{$cat}}">{{$cat}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group row" id="inputExpedition" style="display:none;">
                <label for="inputExpedition" class="col-sm-2 col-form-label">Ekspedisi</label>
                <div class="col-sm-10">
                <input type="text" class="form-control" name="expedition" id="expedition" value="{{$row->expedition}}" placeholder="">
                </div>
            </div>            
            <div class="form-group row">
                <label for="inputName" class="col-sm-2 col-form-label">Nama Pembeli</label>
                <div class="col-sm-10">
                <input type="text" class="form-control" name="name" id="name" value="{{$row->name}}">
                </div>
            </div>
            <div class="form-group row">
                <label for="inputAddress" class="col-sm-2 col-form-label">Alamat</label>
                <div class="col-sm-10">
                <input type="address" class="form-control" name="address" id="address" placeholder="" value="{{$row->address}}">
                </div>
            </div>
            
            <div class="form-group row">
                <label for="inputNote" class="col-sm-2 col-form-label">Catatan</label>
                <div class="col-sm-10">
                <input type="text" class="form-control" name="note" id="note" placeholder="" value="{{$row->note}}">
                </div>
            </div>
            
        </div>

        <table class="table">
            <thead>
                <tr>
                    <th>Nama Produk</th>
                    <th>Belanja</th>
                    <th>Harga</th>
                    <th>Kotak</th>
                    <th>Jumlah (KG)</th>
                    <th>Koli</th>
                    <th>Total Koli (KG)</th>
                    <th>Sub Total (KG)</th>
                </tr>
            </thead>
            <tbody>
                @if(count($products) > 0)
                    @foreach($products as $key => $product)
                    <tr>
                        <input readonly type="hidden" name="data[{{$key}}][products_id]" value="{{$product->id}}"/>
                        <td width="15%"><input readonly type="text" name="data[{{$key}}][products_name]" value="{{$product->name}}" class="form-control" /></td>
                        <td width="10%"><input type="number" name="data[{{$key}}][purchase_price]" value="<?php if(array_key_exists($product->id, $details)){ echo $details[$product->id]->purchase_price; } ?>" class="form-control" /></td>
                        <td width="10%"><input type="number" name="data[{{$key}}][products_price]" value="<?php if(array_key_exists($product->id, $details)){ echo $details[$product->id]->products_price; } else { echo $product->price; } ?>" class="form-control" /></td>
                        <td width="10%"><input type="number" name="data[{{$key}}][pack]" id="pack_{{$key}}" class="form-control" value="<?php if(array_key_exists($product->id, $details)){ echo $details[$product->id]->pack; } ?>" onkeyup="count({{$key}})"  min="0"/></td>
                        <td width="10%"><input type="number" name="data[{{$key}}][qty]" id="qty_{{$key}}" class="form-control"  value="<?php if(array_key_exists($product->id, $details)){ echo $details[$product->id]->qty; } ?>" onkeyup="count({{$key}})" min="0"/></td>
                        <td width="10%"><input type="number" name="data[{{$key}}][koli]" id="koli_{{$key}}" class="form-control" value="<?php if(array_key_exists($product->id, $details)){ echo $details[$product->id]->koli; } ?>"  min="0"/></td>
                        <td width="10%"><input type="number" name="data[{{$key}}][koli_total]" id="koli_total_{{$key}}" class="form-control" value="<?php if(array_key_exists($product->id, $details)){ echo $details[$product->id]->koli_total; } ?>" onkeyup="count({{$key}})" min="0"/></td>
                        <td width="15%"><input type="number" name="data[{{$key}}][sub_total]" id="sub_total_{{$key}}"  value="<?php if(array_key_exists($product->id, $details)){ echo $details[$product->id]->sub_total; } ?>" class="form-control" /></td>
                    </tr>
                    @endforeach
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>
                            <span id="text_grand_total">0 KG</span>                
                        </td>
                    </tr>
                @endif
            </tbody>
        </table>

        <button type="button" id="btnSavePrint" class="btn btn-sm btn-info pull-right" onclick="confirm('{{$url}}','save-print')">Simpan & Print</button>
        <button type="button" id="btnSave" class="btn btn-sm btn-primary pull-right" style="margin-right:10px;" onclick="confirm('{{$url}}','save')">Simpan</button> &nbsp;

      </form>
    </div>
    <div class='panel-footer'>
      <!-- <input type='submit' class='btn btn-primary' value='Save changes'/> -->
    </div>
  </div>
@endsection
@push('bottom')
<script>
    let mainpath = "{{ CRUDBooster::mainpath('') }}";
    let count_product = {{count($products)}};
</script>
<script src="{{asset('assets/js/admin/sales.js')}}"></script>
<script>
    $(document).ready(function(){
        count();
    })

    const confirm = (action,type) =>
    {
        swal({
            title: "Apakah anda yakin?",
            text: "",
            icon: "warning",
            buttons: true,
            dangerMode: true,
			showCancelButton: true,
			confirmButtonText: "Ya",
			cancelButtonText: "Batal",
			closeOnConfirm: false,
			closeOnCancel: false
        },
		function(isConfirm){
           if (isConfirm){
                insert(action,type);
            } else {
                swal("Dibatalkan", "", "error"); 
            }
         });
    }

</script>
@endpush