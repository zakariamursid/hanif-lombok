<!-- First, extends to the CRUDBooster Layout -->
@extends('crudbooster::admin_template')
@section('content')

<style>
    #saleByCategory
    {
        margin-top:50px;
        margin-bottom:50px;
    }
    #saleByProduct
    {
        margin-top:50px;
        margin-bottom:50px;
    }
    #purchaseByProduct
    {
        margin-top:50px;
        margin-bottom:50px;
    }
</style>

<p><a title="Return" href="{{CrudBooster::mainpath('')}}"><i class="fa fa-chevron-circle-left "></i>
                        &nbsp; Kembali ke halaman Barang Keluar</a></p>

  <!-- Your html goes here -->
  <div class='panel panel-default'>
    <div class='panel-heading'>{{$page_title}}</div>
    <div class='panel-body'>
        <form action="">
            <div class="form-group row">
                <div class="col-sm-10">
                    <input required type="date" class="form-control" name="date" id="date" @if($date) value="{{$date}}" @endif placeholder="">
                </div>
                <div class="col-sm-2">
                    <button class="btn btn-sm btn-info">Tampilkan</button>
                    <button class="btn btn-sm btn-success" type="button" onclick="cetak()">Cetak</button>
                </div>
            </div>
        </form>

        <div class="purchase">
            <h1  style="margin-top:100px;margin-bottom:100px;text-align:center;">
                Pembelian
            </h1>
            <!-- @if(count($purchases) > 0) -->
            @foreach($purchases as $row)
                <div style="padding:10px;">
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Nama Penjual</label>
                        <div class="col-sm-10">
                            {{$row->name}}
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Alamat</label>
                        <div class="col-sm-10">
                            {{$row->address}}
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Status</label>
                        <div class="col-sm-10">
                            {{$row->status}}
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Catatan</label>
                        <div class="col-sm-10">
                            {{$row->note}}
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Diinput Pada</label>
                        <div class="col-sm-10">
                            {{dateTimeFormat($row->created_at)}}
                        </div>
                    </div>
                </div>

            @if(count($row->details) > 0)
            <table class="table">
                <thead>
                    <tr>
                        <th>Nama Produk</th>
                        <th>Harga Produk (/KG)</th>
                        <th>Berat Kotor</th>
                        <th>Berat Bersih</th>
                        <th>Total</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($row->details as $detail)
                    <tr>
                        <td>{{$detail->products_name}}</td>
                        <td>{{idrFormat($detail->products_price)}}</td>
                        <td>{{$detail->bruto}} KG</td>
                        <td>{{$detail->netto}} KG</td>
                        <td>{{idrFormat($detail->sub_total_price)}}</td>
                    </tr>
                @endforeach
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>
                            {{idrFormat($row->total_price)}}
                        </td>
                    </tr>
                </tbody>
            </table>
            @endif

            @endforeach
            <!-- @endif -->

            <div id="purchaseByProduct">
                <h4>Total Pembelian Berdasarkan Produk (Berat Bersih)</h4>
                <br>

                <table class="table">
                    <thead>
                        <tr>
                            <th>Nama Produk</th>
                            <th>Berat Bersih</th>
                            <th>Total</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                            $final = 0;
                        @endphp
                        @foreach($products as $product)
                        <tr>
                            <th>{{$product->name}}</th>
                            <td>
                            @if(array_key_exists($product->id, $purchase_total_by_products))
                                {{$purchase_total_by_products[$product->id]->total}}
                            @else
                            0
                            @endif
                            KG
                            </td>
                            <td>
                            @if(array_key_exists($product->id, $purchase_total_by_products))
                                {{idrFormat($purchase_total_by_products[$product->id]->total_price)}}

                                @php
                                    $final += $purchase_total_by_products[$product->id]->total_price;
                                @endphp
                            @else
                            {{idrFormat(0)}}
                            @endif
                            </td>
                        </tr>
                        @endforeach
                        <tr>
                            <td></td>
                            <td></td>
                            <th>{{idrFormat($final)}}</th>
                        </tr>
                    </tbody>
                </table>
            </div>
            
        </div>
        <hr>

        <div class="sales">
        <h1 style="margin-top:100px;margin-bottom:50px;text-align:center;">
            Pengiriman
        </h1>

            <div class="row mt-5 mb-5">
                @foreach($category as $cat)
                    <div class="col-lg-4 col-md-6 col-sm-12">
                        <h4 style="margin-top:50px;margin-bottom:50px;text-align:center;">
                        {{$cat}}
                        </h4>

                        @if(array_key_exists($cat, $sales))
                        @foreach($sales[$cat] as $val)
                        <div style="padding:10px;">
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Nama</label>
                            <div class="col-sm-8">
                                : {{$val->name}}
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Alamat</label>
                            <div class="col-sm-8">
                                : {{$val->address}}
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Diinput Pada</label>
                            <div class="col-sm-8">
                                : {{dateTimeFormat($val->created_at)}}
                            </div>
                        </div>
                        @if($cat == "Sumatera")
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Ekspedisi</label>
                            <div class="col-sm-8">
                                : {{$val->expedition}}
                            </div>
                        </div>                            
                        @endif
                        </div>

                        @if(count($val->details) > 0)
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Nama Produk</th>
                                    <th>Kotak</th>
                                    <th>Jumlah</th>
                                    <th>Koli</th>
                                    <th>Jumlah</th>
                                    <th>Sub Total</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($val->details as $detail)
                                <tr>
                                    <td>{{$detail->products_name}}</td>
                                    <td>{{$detail->pack}}</td>
                                    <td>{{$detail->qty}} KG</td>
                                    <td>{{$detail->koli}}</td>
                                    <td>{{$detail->koli_total}} KG</td>
                                    <td>{{$detail->sub_total}} KG</td>
                                </tr>
                            @endforeach
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td>
                                        {{$val->total}} KG
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        @endif

                        @endforeach
                        @else
                        <center>
                        <b>Tidak ada data</b>
                        </center>
                        @endif

                    </div>
                @endforeach
            </div>

            <div id="saleByCategory">
                <h4>Total Pengiriman Berdasarkan Kategori</h4>
                <br>
                <table class="table">
                    <tbody>
                        @foreach($category as $cat)
                        <tr>
                            <th>{{$cat}}</th>
                            <th>{{$sales_total_by_category[$cat]}} KG</th>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>

            <div id="saleByProduct">
                <h4>Total Pengiriman Berdasarkan Produk</h4>
                <br>

                <table class="table">
                    <tbody>
                        @foreach($products as $product)
                        <tr>
                            <th>{{$product->name}}</th>
                            <th>
                            @if(array_key_exists($product->id, $sales_total_by_products))
                                {{$sales_total_by_products[$product->id]}}
                            @else
                            0
                            @endif
                            KG
                            </th>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>

            <div id="employee">
                <h4>Pekerja yang berangkat pada {{dateFormat($employee_data->date)}}</h4>
                <br>
                <b>{{$employee_data->persons}}</b>
            </div>

            <div id="notes" style="margin-top:50px;">
                <h4>Catatan harian {{dateFormat($employee_data->date)}}</h4>
                <br>
                {!!$notes->content!!}
            </div>

        </div>

        </div>
    <div class='panel-footer'>
      <!-- <input type='submit' class='btn btn-primary' value='Save changes'/> -->
    </div>
  </div>
@endsection
@push('bottom')
<script>
    const cetak = () =>
    {
        let mainpath = "{{ CRUDBooster::mainpath('') }}";
        
        let date = $('#date').val();

        window.location = mainpath + '/print/' +date;
    }
</script>

@endpush