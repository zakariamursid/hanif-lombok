<!DOCTYPE html>
<html>
<head>

	<title>Laporan Transaksi {{dateFormat($employee_data->date)}}</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
<style>
    .table {
  width: 100%;
  max-width: 100%;
  margin-bottom: 1rem;
}

.table th,
.table td {
  padding: 0.75rem;
  vertical-align: top;
  border-top: 1px solid #eceeef;
}

.table thead th {
  vertical-align: bottom;
  border-bottom: 2px solid #eceeef;
}

.table tbody + tbody {
  border-top: 2px solid #eceeef;
}

.table .table {
  background-color: #fff;
}

.table-sm th,
.table-sm td {
  padding: 0.3rem;
}

.table-bordered {
  border: 1px solid #eceeef;
}

.table-bordered th,
.table-bordered td {
  border: 1px solid #eceeef;
}

.table-bordered thead th,
.table-bordered thead td {
  border-bottom-width: 2px;
}

.table-striped tbody tr:nth-of-type(odd) {
  background-color: rgba(0, 0, 0, 0.05);
}

.table-hover tbody tr:hover {
  background-color: rgba(0, 0, 0, 0.075);
}

.table-active,
.table-active > th,
.table-active > td {
  background-color: rgba(0, 0, 0, 0.075);
}

.table-hover .table-active:hover {
  background-color: rgba(0, 0, 0, 0.075);
}

.table-hover .table-active:hover > td,
.table-hover .table-active:hover > th {
  background-color: rgba(0, 0, 0, 0.075);
}

.table-success,
.table-success > th,
.table-success > td {
  background-color: #dff0d8;
}

.table-hover .table-success:hover {
  background-color: #d0e9c6;
}

.table-hover .table-success:hover > td,
.table-hover .table-success:hover > th {
  background-color: #d0e9c6;
}

.table-info,
.table-info > th,
.table-info > td {
  background-color: #d9edf7;
}

.table-hover .table-info:hover {
  background-color: #c4e3f3;
}

.table-hover .table-info:hover > td,
.table-hover .table-info:hover > th {
  background-color: #c4e3f3;
}

.table-warning,
.table-warning > th,
.table-warning > td {
  background-color: #fcf8e3;
}

.table-hover .table-warning:hover {
  background-color: #faf2cc;
}

.table-hover .table-warning:hover > td,
.table-hover .table-warning:hover > th {
  background-color: #faf2cc;
}

.table-danger,
.table-danger > th,
.table-danger > td {
  background-color: #f2dede;
}

.table-hover .table-danger:hover {
  background-color: #ebcccc;
}

.table-hover .table-danger:hover > td,
.table-hover .table-danger:hover > th {
  background-color: #ebcccc;
}

.thead-inverse th {
  color: #fff;
  background-color: #292b2c;
}

.thead-default th {
  color: #464a4c;
  background-color: #eceeef;
}

.table-inverse {
  color: #fff;
  background-color: #292b2c;
}

.table-inverse th,
.table-inverse td,
.table-inverse thead th {
  border-color: #fff;
}

.table-inverse.table-bordered {
  border: 0;
}

.table-responsive {
  display: block;
  width: 100%;
  overflow-x: auto;
  -ms-overflow-style: -ms-autohiding-scrollbar;
}

.table-responsive.table-bordered {
  border: 0;
}

    #saleByCategory
    {
        margin-top:50px;
        margin-bottom:50px;
    }
    #saleByProduct
    {
        margin-top:50px;
        margin-bottom:50px;
    }
    #purchaseByProduct
    {
        margin-top:50px;
        margin-bottom:50px;
    }
    table {
    border-left: 0.01em solid #ccc;
    border-right: 0;
    border-top: 0.01em solid #ccc;
    border-bottom: 0;
    border-collapse: collapse;
}
table :empty{border:none; height:20px;}
		.table-borderless thead
		{
            border:0px;
		}
		.table-borderless tr
		{
            border:0px;
		}
		.table-borderless td
		{
            border:0px;
		}
		.table-borderless th
		{
            border:0px;
		}

        .table th
        {
            text-align:left;
        }

</style>

@php
    ini_set('max_execution_time', '300'); //300 seconds = 5 minutes
    set_time_limit(300);
@endphp

  <!-- Your html goes here -->
  <div class='panel panel-default'>
    <div class='panel-body'>

        <div class="purchase">
            <h1  style="margin-top:0px;margin-bottom:50px;text-align:center;">
                Pembelian
            </h1>
            <!-- @if(count($purchases) > 0) -->
            @foreach($purchases as $row)
                <table class="table table-borderless" style="border:none;">
                    <thead>
                        <tr>
                            <td width="20%">Nama Penjual</td>
                            <td>: {{$row->name}}</td>
                        </tr>
                        <tr>
                            <td>Alamat</td>
                            <td>: {{$row->address}}</td>
                        </tr>
                        <tr>
                            <td>Status</td>
                            <td>: {{$row->status}}</td>
                        </tr>
                        <tr>
                            <td>Catatan</td>
                            <td>: {{$row->note}}</td>
                        </tr>
                        <tr>
                            <td>Diinput pada</td>
                            <td>: 
                            {{dateTimeFormat($row->created_at)}}
                            </td>
                        </tr>
                    </thead>
                </table>


            @if(count($row->details) > 0)
            <table class="table"  style="border:none;margin-top:50px;">
                <thead>
                    <tr>
                        <th>Nama Produk</th>
                        <th>Harga Produk (/KG)</th>
                        <th>Berat Kotor</th>
                        <th>Berat Bersih</th>
                        <th>Total</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($row->details as $detail)
                    <tr>
                        <td>{{$detail->products_name}}</td>
                        <td>{{idrFormat($detail->products_price)}}</td>
                        <td>{{$detail->bruto}} KG</td>
                        <td>{{$detail->netto}} KG</td>
                        <td>{{idrFormat($detail->sub_total_price)}}</td>
                    </tr>
                @endforeach
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>
                            {{idrFormat($row->total_price)}}
                        </td>
                    </tr>
                </tbody>
            </table>
            @endif

            @endforeach
            <!-- @endif -->

            <div id="purchaseByProduct">
                <h4>Total Pembelian Berdasarkan Produk (Berat Bersih)</h4>
                <br>

                <table class="table table-borderless" style="border:none; text-align:left;">
                    <thead>
                        <tr>
                            <th>Nama Produk</th>
                            <th>Berat Bersih</th>
                            <th>Total</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                            $final = 0;
                        @endphp
                        @foreach($products as $product)
                        <tr>
                            <td>{{$product->name}}</td>
                            <td>
                            @if(array_key_exists($product->id, $purchase_total_by_products))
                                {{$purchase_total_by_products[$product->id]->total}}
                            @else
                            0
                            @endif
                            KG
                            </td>
                            <td>
                            @if(array_key_exists($product->id, $purchase_total_by_products))
                                {{idrFormat($purchase_total_by_products[$product->id]->total_price)}}

                                @php
                                    $final += $purchase_total_by_products[$product->id]->total_price;
                                @endphp
                            @else
                            {{idrFormat(0)}}
                            @endif
                            </td>
                        </tr>
                        @endforeach
                        <tr>
                            <td></td>
                            <td></td>
                            <td>{{idrFormat($final)}}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            
        </div>
        <hr>

        <div class="sales">
        <h1 style="margin-top:100px;margin-bottom:50px;text-align:center;">
            Pengiriman
        </h1>

            <div class="row mt-5 mb-5">
                @foreach($category as $cat)
                    <div class="col-lg-4 col-md-6 col-sm-12">
                        <h4 style="margin-top:50px;margin-bottom:50px;text-align:center;">
                        {{$cat}}
                        </h4>

                        @if(array_key_exists($cat, $sales))
                        @foreach($sales[$cat] as $val)

                        <table class="table table-borderless" style="border:none; text-align:left;">
                            <tbody>
                                <tr>
                                    <td width="20%">Nama</td>
                                    <td>: {{$val->name}}</td>
                                </tr>
                                <tr>
                                    <td>Alamat</td>
                                    <td>: {{$val->address}}</td>
                                </tr>
                                <tr>
                                    <td>Diinput Pada</td>
                                    <td>: 
                                     {{dateTimeFormat($val->created_at)}}
                                    </td>
                                </tr>
                                @if($cat == "Sumatera")
                                <tr>
                                    <td>Ekspedisi</td>
                                    <td>: {{$val->expedition}}</td>
                                </tr>
                                @endif

                            </tbody>
                        </table>


                        @if(count($val->details) > 0)
                        <table class="table table-borderless" style="border:none; text-align:left;">
                            <thead>
                                <tr>
                                    <th>Nama Produk</th>
                                    <th>Kotak</th>
                                    <th>Jumlah</th>
                                    <th>Total</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($val->details as $detail)
                                <tr>
                                    <td>{{$detail->products_name}}</td>
                                    <td>{{$detail->pack}}</td>
                                    <td>{{$detail->qty}} KG</td>
                                    <td>{{$detail->sub_total}} KG</td>
                                </tr>
                            @endforeach
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td>
                                        {{$val->total}} KG
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        @endif

                        @endforeach
                        @else
                        <center>
                        <b>Tidak ada data</b>
                        </center>
                        @endif

                    </div>
                @endforeach
            </div>

            <div id="saleByCategory">
                <h4>Total Pengiriman Berdasarkan Kategori</h4>
                <br>
                <table class="table table-borderless" style="border:none; text-align:left;">
                    <tbody>
                        @foreach($category as $cat)
                        <tr>
                            <th width="20%">{{$cat}}</th>
                            <td>{{$sales_total_by_category[$cat]}} KG</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>

            <div id="saleByProduct">
                <h4>Total Pengiriman Berdasarkan Produk</h4>
                <br>

                <table class="table table-borderless" style="border:none; text-align:left;">
                    <tbody>
                        @foreach($products as $product)
                        <tr>
                            <th width="20%">{{$product->name}}</th>
                            <td>
                            @if(array_key_exists($product->id, $sales_total_by_products))
                                {{$sales_total_by_products[$product->id]}}
                            @else
                            0
                            @endif
                            KG
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>

            <div id="employee">
                <h4>Pekerja yang berangkat pada {{dateFormat($employee_data->date)}}</h4>
                <br>
                <b>{{$employee_data->persons}}</b>
            </div>

        </div>

        </div>
  </div>