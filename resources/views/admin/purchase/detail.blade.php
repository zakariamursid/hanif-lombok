<!-- First, extends to the CRUDBooster Layout -->
@extends('crudbooster::admin_template')
@section('content')

<p><a title="Return" href="{{CrudBooster::mainpath('')}}"><i class="fa fa-chevron-circle-left "></i>
                        &nbsp; Kembali ke halaman Barang Masuk</a></p>

  <!-- Your html goes here -->
  <div class='panel panel-default'>
    <div class='panel-heading'>{{$page_title}}</div>
    <div class='panel-body'>
        <div style="padding:10px;">

            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Nama Penjual</label>
                <div class="col-sm-10">
                    {{$row->name}}
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Alamat</label>
                <div class="col-sm-10">
                    {{$row->address}}
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Status</label>
                <div class="col-sm-10">
                    {{$row->status}}
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Catatan</label>
                <div class="col-sm-10">
                    {{$row->note}}
                </div>
            </div>

            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Diinput Pada</label>
                <div class="col-sm-10">
                    {{dateTimeFormat($row->created_at)}}
                </div>
            </div>
        </div>

            <table class="table">
            <thead>
                <tr>
                    <th>Nama Produk</th>
                    <th>Harga Produk (/KG)</th>
                    <th>Berat Kotor</th>
                    <th>Berat Bersih</th>
                    <th>Total</th>
                </tr>
            </thead>
            <tbody>
                @if(count($row->details) > 0)
                    @foreach($row->details as $key => $val)
                    <tr>
                        <td width="20%">{{$val->products_name}}</td>
                        <td width="20%">{{idrFormat($val->products_price)}}</td>
                        <td width="20%">{{$val->bruto}} KG</td>
                        <td width="20%">{{$val->netto}} KG</td>
                        <td width="20%">{{idrFormat($val->sub_total_price)}}</td>
                    </tr>
                    @endforeach
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>
                            {{idrFormat($row->total_price)}}
                        </td>
                    </tr>
                @endif
            </tbody>
        </table>

        <a onclick="confirm('Apakah anda yakin?')" class="btn btn-sm btn-info" href="{{CRUDBooster::mainpath('print/'.$row->id)}}">PRINT</a>
    </div>
  </div>
@endsection
