<!-- First, extends to the CRUDBooster Layout -->
@extends('crudbooster::admin_template')
@section('content')

<p><a title="Return" href="{{CrudBooster::mainpath('')}}"><i class="fa fa-chevron-circle-left "></i>
                        &nbsp; Kembali ke halaman Barang Masuk</a></p>

  <!-- Your html goes here -->
  <div class='panel panel-default'>
    <div class='panel-heading'>{{$page_title}}</div>
    <div class='panel-body'>
      <form method='post' id="form">
        <div style="padding:10px;">
            <input type="hidden" class="form-control" name="id" id="id" value="{{$row->id}}">
            <div class="form-group row">
                <label for="inputName" class="col-sm-2 col-form-label">Nama Penjual</label>
                <div class="col-sm-10">
                <input type="text" class="form-control" name="name" id="name" value="{{$row->name}}">
                </div>
            </div>
            <div class="form-group row">
                <label for="inputAddress" class="col-sm-2 col-form-label">Alamat</label>
                <div class="col-sm-10">
                <input type="address" class="form-control" name="address" id="address" value="{{$row->address}}">
                </div>
            </div>
            <div class="form-group row">
                <label for="inputNote" class="col-sm-2 col-form-label">Catatan</label>
                <div class="col-sm-10">
                <input type="text" class="form-control" name="note" id="note" placeholder="" value="{{$row->note}}">
                </div>
            </div>
            <div class="form-group row">
                <label for="inputStatus" class="col-sm-2 col-form-label">Status</label>
                <div class="col-sm-10">
                    <select name="status" id="status" class="form-control">
                        @foreach($status as $st)
                            <option @if(isset($row)) @if($row->status == $st) selected @endif @endif value="{{$st}}">{{$st}}</option>
                        @endforeach
                    </select>
                </div>
            </div>                    
        </div>

        <table class="table">
            <thead>
                <tr>
                    <th>Nama Produk</th>
                    <th>Harga Produk (/KG)</th>
                    <th>Berat Kotor</th>
                    <th>Berat Bersih</th>
                    <th>Total</th>
                </tr>
            </thead>
            <tbody>
                @if(count($products) > 0)
                    @foreach($products as $key => $product)
                    <tr>
                        <input readonly type="hidden" name="data[{{$key}}][products_id]" value="{{$product->id}}"/>
                        <td width="20%"><input readonly type="text" name="data[{{$key}}][products_name]" placeholder="Nama Produk" value="{{$product->name}}" class="form-control" /></td>
                        <td width="20%"><input type="number" name="data[{{$key}}][products_price]" id="products_price_{{$key}}" placeholder="Harga Produk" value="<?php if(array_key_exists($product->id, $details)){ echo $details[$product->id]->products_price; } else { echo $product->price; } ?>" class="form-control" onkeyup="count({{$key}})"/></td>
                        <td width="20%"><input type="number" min="0" step="any" name="data[{{$key}}][bruto]" id="bruto_{{$key}}" placeholder="Berat Kotor" value="<?php if(array_key_exists($product->id, $details)){ echo $details[$product->id]->bruto; } ?>" class="form-control" onkeyup="tara({{$key}})"/></td>
                        <td width="20%"><input type="number" min="0" step="any"  name="data[{{$key}}][netto]" id="netto_{{$key}}" placeholder="Berat Bersih" class="form-control" onkeyup="count({{$key}})" value="<?php if(array_key_exists($product->id, $details)){ echo $details[$product->id]->netto; } ?>"/></td>
                        <td width="20%"><input type="number" min="0" name="data[{{$key}}][sub_total_price]" id="sub_total_price_{{$key}}" placeholder="Total" class="form-control" value="<?php if(array_key_exists($product->id, $details)){ echo $details[$product->id]->sub_total_price; } ?>"/></td>
                    </tr>
                    @endforeach
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>
                            <span id="text_grand_total">Rp. 0</span>                
                        </td>
                    </tr>
                @endif
            </tbody>
        </table>

        <button type="button" id="btnSavePrint" class="btn btn-sm btn-info pull-right" onclick="confirm('{{$url}}','save-print')">Simpan & Print</button>
        <button type="button" id="btnSave" class="btn btn-sm btn-primary pull-right" style="margin-right:10px;" onclick="confirm('{{$url}}','save')">Simpan</button> &nbsp;

      </form>
    </div>
    <div class='panel-footer'>
      <!-- <input type='submit' class='btn btn-primary' value='Save changes'/> -->
    </div>
  </div>
@endsection
@push('bottom')
<script>
let mainpath = "{{ CRUDBooster::mainpath('') }}";
let count_product = {{count($products)}};

</script>
<script src="{{asset('assets/js/admin/purchase.js')}}"></script>
<script>
    $(document).ready(function(){
        count();
    })

    const confirm = (action,type) =>
    {
        swal({
            title: "Apakah anda yakin?",
            text: "",
            icon: "warning",
            buttons: true,
            dangerMode: true,
            showCancelButton: true,
            confirmButtonText: "Ya",
            cancelButtonText: "Batal",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function(isConfirm)
        {
            if (isConfirm)
            {
                insert(action,type);
            }
            else
            {
                swal("Dibatalkan", "", "error"); 
            }
        });
    }
</script>
@endpush