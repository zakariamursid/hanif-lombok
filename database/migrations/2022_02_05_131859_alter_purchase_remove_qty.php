<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterPurchaseRemoveQty extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('purchase', function (Blueprint $table) {
            $table->dropColumn('qty');
            $table->string('status');
        });
        Schema::table('purchase_detail', function (Blueprint $table) {
            $table->integer('products_id');
            $table->integer('purchase_id');
            $table->dropColumn('sales_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('purchase', function (Blueprint $table) {
            $table->integer('qty');
            $table->dropColumn('status');
        });
        Schema::table('purchase_detail', function (Blueprint $table) {
            $table->dropColumn('products_id');
            $table->dropColumn('purchase_id');
            $table->integer('sales_id');
        });
    }
}
