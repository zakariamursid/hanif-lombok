<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterSalesRemoveTotalPrice extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sales_detail', function (Blueprint $table) {
            $table->dropColumn('netto');
            $table->dropColumn('bruto');

            $table->integer('pack');
            $table->integer('qty');
            $table->integer('sub_total');
        });

        Schema::table('purchase_detail', function (Blueprint $table) {
            $table->float('netto')->change();
            $table->float('bruto')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sales_detail', function (Blueprint $table) {
            $table->float('netto');
            $table->float('bruto');

            $table->dropColumn('pack');
            $table->dropColumn('qty');
            $table->dropColumn('sub_total');
        });

        Schema::table('purchase_detail', function (Blueprint $table) {
            $table->integer('netto')->change();
            $table->integer('bruto')->change();
        });
    }
}
