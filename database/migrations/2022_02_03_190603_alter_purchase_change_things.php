<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterPurchaseChangeThings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('purchase', function (Blueprint $table) {
            $table->dropColumn('seller_name');
            $table->dropColumn('seller_address');
            $table->dropColumn('products_id');
            $table->dropColumn('price');

            $table->string('name');
            $table->longText('address');
            $table->bigInteger('total_price');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('purchase', function (Blueprint $table) {
            $table->string('seller_name');
            $table->string('seller_address');
            $table->integer('price');
            $table->integer('products_id');

            $table->dropColumn('name');
            $table->dropColumn('address');
            $table->dropColumn('total_price');
        });
    }
}
