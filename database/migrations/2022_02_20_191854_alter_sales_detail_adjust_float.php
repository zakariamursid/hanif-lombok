<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterSalesDetailAdjustFloat extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sales_detail', function (Blueprint $table) {
            $table->float('pack')->change();
            $table->float('qty')->change();
            $table->float('sub_total')->change();
        });
        Schema::table('sales', function (Blueprint $table) {
            $table->float('total')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sales_detail', function (Blueprint $table) {
            $table->integer('pack')->change();
            $table->integer('qty')->change();
            $table->integer('sub_total')->change();
        });

        Schema::table('sales', function (Blueprint $table) {
            $table->integer('total')->change();
        });
    }
}
